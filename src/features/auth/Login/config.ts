import {
  FormConfig,
  FormErrors,
  FormFieldTypeMap, FormValues,
  TextInputType,
} from "components/Form/types";
import validateEmail from "lib/validators/email";


export type LoginData={
  email: string,
  password: string
}

export type FormFiledConfig = {
  id: string;
  placeholder: string;
  type: TextInputType;
  required: boolean | undefined;
};
export const config: FormConfig = [
  {
    id: "email",
    type: "inputEmail",
    label: "someemail@example.com",
    required: true,
  },
  {
    id: "password",
    type: "inputPassword",
    label: "Введите пароль",
    required: true,
  },
];

export const getFields = (config: FormConfig): FormFiledConfig[] =>
  config.map((field) => ({
    id: field.id,
    required: field.required,
    placeholder: field.label,
    type: FormFieldTypeMap[field.type] as TextInputType,
  }));

export const LOGIN_DIALOG = {
  icon: "💁‍♀️",
  title: "Авторизация",
  subtitle:
    "Для доступа к личному кабинету вашей компании авторизуйтесь на сайте",
};

export const FIELDS = getFields(config);
export const CLEARABLE = ["email"];

export const validate = (values: FormValues, errors:FormErrors): FormErrors => {
  if (values.email) {
    if (!validateEmail(values.email)) {
      errors.email =
        values.email === undefined
          ? ["Invalid email"]
          : [...values.email, "Invalid email"];
    }
  }
  return errors;
};
