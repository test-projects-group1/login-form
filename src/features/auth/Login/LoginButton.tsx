import Button from "components/Button/Button";
import { useFormContext } from "components/Form/context";


const LoginButton = () => {
  const { isValid } = useFormContext();
  return (
    <Button type="submit" variant="filled" color="primary" size="medium" disabled={!isValid}>
      Войти
    </Button>
  );
};

export default LoginButton;
