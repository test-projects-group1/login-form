import { useCallback } from "react";
import { Dialog, DialogTitle, DialogActions } from "components/Dialog";
import { Column, Field, Form, Row } from "components/Form";
import { FormValues } from "components/Form/types";

import {login} from "./api";
import { config, FIELDS, LOGIN_DIALOG, CLEARABLE, validate } from "./config";
import LoginButton from "./LoginButton";


const LoginForm = () => {
  const handleLogin = useCallback((values: FormValues) => {
    login(values);
  }, []);
  return (
    <Dialog open={true}>
      <DialogTitle icon={LOGIN_DIALOG.icon} subtitle={LOGIN_DIALOG.subtitle}>
        {LOGIN_DIALOG.title}
      </DialogTitle>
      <Form handleSubmit={handleLogin} config={config} validate={validate}>
        {FIELDS.map((field) => (
          <Row key={field.id}>
            <Column>
              <Field
                {...field}
                size="medium"
                clearable={CLEARABLE.includes(field.id)}
              />
            </Column>
          </Row>
        ))}
        <DialogActions>
          <LoginButton />
        </DialogActions>
      </Form>
    </Dialog>
  );
};

export default LoginForm;
