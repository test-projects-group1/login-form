import {render, screen, waitFor} from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import * as api from "./api";
import LoginForm from "./LoginForm";



const login = jest.spyOn(api, "login");
const EMAIL = "some@email.com";
  const PASSWORD = "change_me";

const fillForm = async (email=EMAIL, password=PASSWORD) => {
  expect(screen.getByText("Войти")).toBeEnabled();
  await userEvent.type(
    screen.getByPlaceholderText("someemail@example.com"),
    email
  );
  await userEvent.type(screen.getByPlaceholderText("Введите пароль"), password);
};

describe("LoginForm", () => {
  it("should render default all elements", () => {
    render(<LoginForm />);
    expect(screen.getByText("💁‍♀️")).toBeInTheDocument();
    expect(screen.getByText("Авторизация")).toBeInTheDocument();
    expect(
      screen.getByText(
        "Для доступа к личному кабинету вашей компании авторизуйтесь на сайте"
      )
    ).toBeInTheDocument();
    expect(
      screen.getByPlaceholderText("someemail@example.com")
    ).toBeInTheDocument();
    expect(screen.getByPlaceholderText("Введите пароль")).toBeInTheDocument();
    expect(screen.getByText("Войти")).toBeInTheDocument();
  });
  it("should handle submit", async () => {
    render(<LoginForm />);
    await fillForm(EMAIL, PASSWORD)
    expect(screen.getByText("Войти")).toBeEnabled();
    await userEvent.click(screen.getByText("Войти"));
    expect(login).toHaveBeenCalledWith({ email:EMAIL, password:PASSWORD });
  });
  it("email required", async () => {
    render(<LoginForm />);
    await fillForm()
    expect(screen.getByText("Войти")).toBeEnabled();
    await userEvent.clear(screen.getByPlaceholderText("someemail@example.com"));
    expect(screen.getByText("Войти")).toBeDisabled();
  });
  it("password required", async () => {
    render(<LoginForm />);
    await fillForm()
    expect(screen.getByText("Войти")).toBeEnabled();
    await userEvent.clear(screen.getByPlaceholderText("Введите пароль"));
    expect(screen.getByText("Войти")).toBeDisabled();
  });
});
