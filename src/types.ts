import { ButtonHTMLAttributes } from "react";


export type ReactNodeChildren = {
  children: React.ReactNode;
};

export type Size = "large" | "medium" | "small";
export type Variant = "filled" | "outline" | "text";
export type Color = "primary" | "secondary" | "default";

export type ButtonProps = ButtonHTMLAttributes<HTMLButtonElement> & {
  className?: string;
  size?: Size;
  variant?: Variant;
  color?: Color;
};
