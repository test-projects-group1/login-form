import { render, screen } from "@testing-library/react";
import Button from "./Button";


describe("Button", () => {
  it("should render default props", () => {
    render(
      <Button>
        Ok
      </Button>
    );
    expect(screen.getByText("Ok")).toHaveClass("Button-text-secondary");
    expect(screen.getByText("Ok")).toHaveClass("Button-medium");
  });
  it("should render passed props", () => {
    render(
      <Button variant="filled" color="primary" size="large">
        Ok
      </Button>
    );
    expect(screen.getByText("Ok")).toHaveClass("Button-filled-primary");
    expect(screen.getByText("Ok")).toHaveClass("Button-large");
  });
});
