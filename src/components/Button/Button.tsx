import React from "react";
import clsx from "clsx";
import { ButtonProps } from "types";
import styles from "./Button.module.scss";


const Button = ({
  children,
  size = "medium",
  variant = "text",
  color = "secondary",
  ...props
}: ButtonProps) => {
  return (
    <button
      {...props}
      className={clsx(
        styles.Button,
        styles[`Button-${variant}-${color}`],
        styles[`Button-${size}`]
      )}
    >
      {children}
    </button>
  );
};

export default Button;
