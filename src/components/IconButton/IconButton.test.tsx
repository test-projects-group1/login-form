import { render, screen } from "@testing-library/react";
import IconButton from "./IconButton";


describe("IconButton", () => {
  it("should render default props", () => {
    render(
      <IconButton>
        Ok
      </IconButton>
    );
    expect(screen.getByText("Ok")).toHaveClass("IconButton-text-default");
    expect(screen.getByText("Ok")).toHaveClass("IconButton-medium");
  });
  it("should render passed props", () => {
    render(
      <IconButton variant="filled" color="primary" size="large">
        Ok
      </IconButton>
    );
    expect(screen.getByText("Ok")).toHaveClass("IconButton-filled-primary");
    expect(screen.getByText("Ok")).toHaveClass("IconButton-large");
  });
});
