import clsx from "clsx";
import { ButtonProps } from "types";
import styles from "./IconButton.module.scss";


const IconButton = ({
  size = "medium",
  variant = "text",
  color = "default",
  className,
  ...props
}: ButtonProps) => {
  return (
    <button
      className={clsx(
        className,
        styles.IconButton,
        styles[`IconButton-${variant}-${color}`],
        styles[`IconButton-${size}`]
      )}
      {...props}
    />
  );
};

export default IconButton;
