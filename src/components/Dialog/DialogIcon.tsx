import styles from "./DialogIcon.module.scss";


export type DialogIconProps = {
  children: React.ReactNode;
};
const DialogIcon = ({ children }: DialogIconProps) => {
  return <div className={styles.DialogIcon}>{children}</div>;
};

export default DialogIcon;
