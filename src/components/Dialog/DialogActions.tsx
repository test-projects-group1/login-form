import clsx from "clsx";
import styles from "./DialogActions.module.scss";


export type DialogActionsProps = {
  children: React.ReactNode;
};
const DialogActions = ({ children }: DialogActionsProps) => {
  return <div className={clsx(styles.DialogActions)}>{children}</div>;
};

export default DialogActions;
