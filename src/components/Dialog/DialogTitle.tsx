import clsx from "clsx";
import styles from "./DialogTitle.module.scss";
import { DialogIcon } from "./index";


export type DialogTitleProps = {
  children: React.ReactNode;
  subtitle?: string;
  icon?: string;
};
const DialogTitle = ({ children, subtitle, icon }: DialogTitleProps) => {
  return (
    <>
      <div
        className={clsx(styles.DialogTitle, { [styles[`DialogTitle-with-icon`]]: icon })}
      >
        {children}
      </div>
      {icon ? <DialogIcon>{icon}</DialogIcon> : null}
      {subtitle ? (
        <div className={styles[`DialogTitle-subtitle`]}>{subtitle}</div>
      ) : null}
    </>
  );
};

export default DialogTitle;
