import clsx from "clsx";
import { Size } from "types";
import Paper from "components/Papper";
import styles from "./Dialog.module.scss";


export type DialogProps = {
  open?: boolean;
  size?: Size;
  children: React.ReactNode | React.ReactNode[];
};
const Dialog = ({ open = false, size = "medium", children }: DialogProps) => {
  return (
    <div
      className={clsx(styles.Dialog, styles[`Dialog-${size}`], {
        [`${styles.Dialog}-open`]: open,
      })}
    >
      <Paper>{children}</Paper>
    </div>
  );
};

export default Dialog;
