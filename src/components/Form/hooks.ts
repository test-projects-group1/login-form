import { ChangeEvent, useCallback, useState } from "react";
import { useFormContext } from "./context";
import { getInitialFormState, validateRequired } from "./lib";
import { FormProps } from "./types";


export const useTextField = (name: string) => {
  const { values, onInputChange } = useFormContext();
  const clear = useCallback(() => {
    onInputChange(name, "");
  }, [name, onInputChange]);
  const onChange = useCallback(
    (e: ChangeEvent<HTMLInputElement>) => {
      onInputChange(name, e.target.value);
    },
    [name, onInputChange]
  );
  return {
    value: values[name],
    onChange,
    clear,
  };
};

export const useForm = ({
  config,
  validate,
  handleSubmit,
}: Omit<FormProps, "children">) => {
  const [{ values, errors, required }, setState] = useState(
    getInitialFormState(config)
  );
  const onInputChange = useCallback(
    (name: string, value: string) => {
      setState((currentState) => {
        const nextValues = {
          ...currentState.values,
          [name]: value,
        };
        let nextErrors = validateRequired({ values:nextValues, errors:{}, required });
        nextErrors = validate ? validate(nextValues, nextErrors) : nextErrors;

        return { ...currentState, values: nextValues, errors: nextErrors };
      });
    },
    [required, validate]
  );
  const onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    handleSubmit(values);
  };

  return {
    isValid: Object.keys(errors).length === 0,
    values,
    onSubmit,
    onInputChange,
  };
};
