import React, { useMemo } from "react";
import clsx from "clsx";
import { useTextField } from "../hooks";
import FormInput from "../TextInput/TextInput";
import { TextFieldProps } from "../types";
import ClearButton from "./ClearButton";
import styles from "./TextField.module.scss";


export const TextField = ({
  label,
  id,
  clearable,
  ...inputProps
}: TextFieldProps) => {
  return (
    <div
      className={clsx(
        styles.TextField,
        styles[`TextField-${inputProps.size}`],
        {
          [styles[`TextField-label`]]: label,
          [styles[`TextField-disabled`]]: inputProps.disabled,
        }
      )}
    >
      {label ? <label htmlFor={`field-${id}`}>{label}</label> : null}
      <FormInput id={id} name={id} {...inputProps} />
    </div>
  );
};

export const TextFieldContainer = (props: TextFieldProps) => {
  const { id, clearable, endComponent, size } = props;
  const { value, onChange, clear } = useTextField(id);

  const endComp = useMemo(() => {
    return (
      <>
        {clearable ? <ClearButton clear={clear} size={size} /> : null}
        {endComponent}
      </>
    );
  }, [clear, clearable, endComponent, size]);



  return (
    <TextField
      {...props}
      value={value}
      onChange={onChange}
      endComponent={endComp}
    />
  );
};

export default TextFieldContainer;
