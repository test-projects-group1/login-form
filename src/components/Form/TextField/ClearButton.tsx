import React from "react";
import IconButton from "components/IconButton";
import { CloseIcon } from "components/Icons";
import { TextFieldProps } from "../types";


export type ClearButtonProps = Pick<TextFieldProps, "size"> & {
  clear: () => void;
};

const ClearButton = ({ size, clear }: ClearButtonProps) => {
  return (
    <IconButton onClick={clear} size={size}>
      <CloseIcon />
    </IconButton>
  );
};

export default ClearButton;
