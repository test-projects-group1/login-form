import FormContext from "../context";
import { useForm } from "../hooks";
import { FormProps } from "../types";


const Form: React.FC<FormProps> = ({
  children,
  config,
  handleSubmit,
  validate,
}) => {
  const { onInputChange, values, onSubmit, isValid } = useForm({
    config,
    handleSubmit,
    validate,
  });
  return (
    <FormContext.Provider value={{ values, onInputChange, isValid }}>
      <form onSubmit={onSubmit}>{children}</form>
    </FormContext.Provider>
  );
};

export default Form;
