import React from "react";
import clsx from "clsx";
import { TextInputProps } from "../types";
import styles from "./TextInput.module.scss";


const TextInput = ({
  size = "medium",
  variant = "outline",
  endComponent,
  startComponent,
  ...inputProps
}: TextInputProps) => {
  return (
    <div
      className={clsx(
        styles.TextInput,
        styles[`TextInput-${size}`],
        styles[`TextInput-${variant}`]
      )}
    >
      {startComponent ? (
        <div className={styles[`TextInput-startComponent`]}>
          {startComponent}
        </div>
      ) : null}

      <input {...inputProps} />

      {endComponent ? (
        <div className={styles[`TextInput-endComponent`]}>{endComponent}</div>
      ) : null}
    </div>
  );
};
export default TextInput;
