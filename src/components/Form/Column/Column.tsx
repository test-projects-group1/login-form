import { ReactNodeChildren } from "types";
import styles from "./Column.module.scss";


const FormColumn = ({ children }: ReactNodeChildren) => {
  return <div className={styles.FormColumn}>{children}</div>;
};

export default FormColumn;
