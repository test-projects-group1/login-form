import { FormConfig, FormErrors, FormRequired, FormValues } from "./types";


export const getInitialValues = (config: FormConfig): FormValues =>
  config.reduce(
    (values, field) => ({ ...values, [field.id]: field.defaultValue || "" }),
    {}
  );

const INITIAL_FORM_STATE = {
  values: {},
  errors: {},
  required: {},
} as const;

type FormState = {
  values: FormValues;
  errors: FormErrors;
  required: FormRequired;
};

export const getInitialFormState = (
  config: FormConfig
): typeof INITIAL_FORM_STATE =>
  config.reduce(
    (state, field) => ({
      ...state,
      values: { ...state.values, [field.id]: field.defaultValue || "" },
      required: { ...state.required, [field.id]: Boolean(field.required) },
    }),
    INITIAL_FORM_STATE
  );
const REQUIRED_ERROR = "Required field";
export const validateRequired = ({
  values,
  required,
  errors,
}: FormState): FormErrors => {
  return Object.keys(values).reduce((errors, id) => {
    const isEmptyRequiredValue = required[id] && !values[id]
    if (isEmptyRequiredValue) {
      return {
        ...errors,
        [id]:
          errors[id] === undefined
            ? [REQUIRED_ERROR]
            : [...errors[id], REQUIRED_ERROR],
      };
    }
    return errors;
  }, errors);
};
