import React, {InputHTMLAttributes} from "react";

import {Size, Variant} from "../../types";


export type ReactNodeChildren = {
    children: React.ReactNode;
};

export type FormFieldType = "inputText" | "inputEmail" | "inputPassword";
export type TextInputType = "text" | "email" | "password";

export const FormFieldTypeMap = {
    inputText: "text",
    inputEmail: "email",
    inputPassword: "password",
};

export type FormField = {
    id: string;
    type: FormFieldType;
    label: string;
    required?: boolean;
    defaultValue?: string;
};

export type FormConfig = FormField[];

export type InputAdditionalComponent =
    | React.ReactNode
    | React.ReactNode[]
    | string;

export type TextInputProps = Omit<
    InputHTMLAttributes<HTMLInputElement>,
    "size"
> & {
    variant?: Variant;
    type: TextInputType;
    size: Size;
    clearable?: boolean;
    startComponent?: InputAdditionalComponent;
    endComponent?: InputAdditionalComponent;
};

export type TextFieldProps = TextInputProps & {
    id: string;
    label?: string;
};

export type FormValues = any;

export type FormErrors = any

export type FormRequired = any

export type FormProps = ReactNodeChildren & {
    config: FormConfig,
    handleSubmit: (values: FormValues) => void;
    validate?: (values: FormValues, errors:FormErrors) => FormErrors
};
