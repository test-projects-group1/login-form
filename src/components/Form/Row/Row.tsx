import { ReactNodeChildren } from "types";
import styles from "./Row.module.scss";


const FormRow = ({ children }: ReactNodeChildren) => {
  return <div className={styles.FormRow}>{children}</div>;
};

export default FormRow;
