import { createContext, useContext } from "react";
import { FormValues } from "./types";


export type FormContextValue = {
  values: FormValues;
  onInputChange: (id: string, value: string) => void;
  isValid: boolean;
};

const FormContext = createContext<FormContextValue | undefined>(undefined);

export const useFormContext = () => {
  const value = useContext(FormContext);
  if (!value)
    throw Error("useFormContext should be used inside FormContext.Provider");
  return value;
};

export default FormContext;
