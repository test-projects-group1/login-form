import clsx from "clsx";
import styles from "./Paper.module.scss";


export type PaperProps = {
  elevation?: 1 | 2 | 3 | "1" | "2" | "3";
  children: React.ReactNode | React.ReactNode[];
};
const Paper = ({ elevation = 1, children }: PaperProps) => {
  return (
    <div className={clsx(styles.Paper, styles[`Paper-elevation-${elevation}`])}>
      {children}
    </div>
  );
};

export default Paper;
